package com.design.patterns.structural.flyweight;

public interface Bird {
    void draw();
}
