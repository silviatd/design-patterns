package com.design.patterns.structural.flyweight;

public class Main {
    private static final String colors[] = {"Red", "Orange", "Blue", "Yellow", "Pink"};

    public static void main(String[] args) {
        for (int i = 0; i < 20; ++i) {
            Bird bird = BirdFactory.createAngryBird(getRandomColor());
            bird.draw();
        }
    }

    private static String getRandomColor() {
        return colors[(int) (Math.random() * colors.length)];
    }
}
