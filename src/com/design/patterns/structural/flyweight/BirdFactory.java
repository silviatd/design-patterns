package com.design.patterns.structural.flyweight;

import java.util.HashMap;
import java.util.Map;

public class BirdFactory {
    /*
     * The Flyweight design pattern can be used exclusively to lower the number of generated objects
     * and even eliminate memory density and increase performance. This particular type of design pattern
     * is structural in nature even though this pattern wants to offer ways to minimise the range of objects
     * and even to strengthen the architecture of the object of the application.
     *
     * However in the Flyweight
     * pattern, we have used a Hash-map that stores references to the already created object, each and every
     * object is associated with a key. If a client wanted to create an object, he simply moves a key normally
     * associated with it unless the object is already being created, we simply just get the direct reference
     * to that object, otherwise it introduces a new object and then reverts it to the client.
     * */
    private static final Map<String, Bird> map = new HashMap<>();

    public static Bird createAngryBird(String color) {
        Bird bird = map.get(color);

        if (bird == null) {
            bird = new AngryBird(color);
            map.put(color, bird);
            System.out.println("create color : " + color);
        }
        return bird;
    }
}
