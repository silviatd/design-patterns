## Flyweight Design Patterns

The flyweight pattern is a Gang of Four design pattern. This is a structural pattern as it defines a manner for creating relationships between classes. The flyweight design pattern is used to reduce the memory and resource usage for complex models containing many hundreds, thousands or even hundreds of thousands of similar objects.