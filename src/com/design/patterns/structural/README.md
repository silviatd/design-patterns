## Structural Design Patterns

Structural design patterns show you how to glue different pieces of a system together in a flexible and extensible fashion. They help you guarantee that when one of the parts changes, the entire structure does not need to change.

These patterns focus on, how the classes inherit from each other and how they are composed from other classes. Structural patterns use inheritance to compose interface or implementations. Structural object patterns describe ways to compose objects to realize new functionality