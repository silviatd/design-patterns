package com.design.patterns.structural.decorator.ice.cream;

public class Main {
    public static void main(String args[]) {
        IceCream iceCream = new SimpleIceCream();
        System.out.println(iceCream.makeIceCream());

        IceCream iceCreamNut = new NuttyDecorator(new SimpleIceCream());
        System.out.println(iceCreamNut.makeIceCream());

        IceCream iceCreamHoney = new HoneyDecorator(new SimpleIceCream());
        System.out.println(iceCreamHoney.makeIceCream());

        IceCream iceCreamHoneyNut = new HoneyDecorator(new NuttyDecorator(new SimpleIceCream()));
        System.out.println(iceCreamHoneyNut.makeIceCream());
    }
}
