package com.design.patterns.structural.decorator.ice.cream;

public class SimpleIceCream implements IceCream {
    @Override
    public String makeIceCream() {
        return "ice cream";
    }

}