package com.design.patterns.structural.decorator.ice.cream;

public interface IceCream {
    String makeIceCream();
}