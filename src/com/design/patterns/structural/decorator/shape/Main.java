package com.design.patterns.structural.decorator.shape;

public class Main {
    /*
    * Decorator pattern allows a user to add new functionality to an existing
    * object without altering its structure. This type of design pattern comes
    * under structural pattern as this pattern acts as a wrapper to existing class.
    * This pattern creates a decorator class which wraps the original class and provides
    * additional functionality keeping class methods signature intact.
    * */
    public static void main(String[] args) {
        Shape circle = new Circle();
        circle.draw();
        System.out.println("---");

        Shape redCircle = new RedShapeDecorator(new Circle());
        redCircle.draw();
        System.out.println("---");

        Shape redRectangle = new RedShapeDecorator(new Rectangle());
        redRectangle.draw();
    }
}
