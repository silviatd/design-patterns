package com.design.patterns.creational.singleton;

public class EagerSingleton {
    private static final EagerSingleton instance = new EagerSingleton();

    private EagerSingleton() {
        // Empty constructor
    }

    public static EagerSingleton getInstance() {
        return instance;
    }
}
