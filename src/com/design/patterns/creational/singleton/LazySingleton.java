package com.design.patterns.creational.singleton;

public class LazySingleton {
    private static LazySingleton instance;

    private LazySingleton() {
        // Empty constructor
    }

    public static LazySingleton getInstance() {
        if (instance == null) {
            instance = new LazySingleton();
        }

        return instance;
    }
}
