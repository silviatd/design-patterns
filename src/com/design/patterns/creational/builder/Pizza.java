package com.design.patterns.creational.builder;

import com.design.patterns.creational.builder.enums.Cheese;
import com.design.patterns.creational.builder.enums.Dough;
import com.design.patterns.creational.builder.enums.Size;
import com.design.patterns.creational.builder.enums.Topping;

public class Pizza {
    // required parameters
    private Size size;
    private Dough dough;
    private double price;

    // optional parameters
    private Cheese cheese;
    private Topping topping;

    public static PizzaBuilder builder(Size size, Dough dough, double price) {
        return new PizzaBuilder(size, dough, price);
    }

    private Pizza(PizzaBuilder builder) {
        this.size = builder.size;
        this.dough = builder.dough;
        this.price = builder.price;
        this.cheese = builder.cheese;
        this.topping = builder.topping;
    }

    public Size getSize() {
        return size;
    }

    public Dough getDough() {
        return dough;
    }

    public double getPrice() {
        return price;
    }

    public Cheese getCheese() {
        return cheese;
    }

    public Topping getTopping() {
        return topping;
    }

    // Builder Class
    public static class PizzaBuilder {
        // required parameters
        private Size size;
        private Dough dough;
        private double price;

        // optional parameters
        private Cheese cheese;
        private Topping topping;

        public PizzaBuilder(Size size, Dough dough, double price) {
            this.size = size;
            this.dough = dough;
            this.price = price;
        }

        public PizzaBuilder setCheese(Cheese cheese) {
            this.cheese = cheese;
            return this;
        }

        public PizzaBuilder setTopping(Topping topping) {
            this.topping = topping;
            return this;
        }

        public Pizza build() {
            return new Pizza(this);
        }
    }
}
