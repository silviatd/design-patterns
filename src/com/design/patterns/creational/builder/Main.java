package com.design.patterns.creational.builder;

import com.design.patterns.creational.builder.enums.Dough;
import com.design.patterns.creational.builder.enums.Size;
import com.design.patterns.creational.builder.enums.Cheese;

public class Main {

    public static void main(String[] args) {
        Pizza pizza = Pizza.builder(Size.LARGE, Dough.ITALIAN, 10)
                .setCheese(Cheese.AMERICAN)
                .build();
    }
}
