package com.design.patterns.creational.builder.enums;

public enum Size {
    SMALL,
    MEDIUM,
    LARGE
}
