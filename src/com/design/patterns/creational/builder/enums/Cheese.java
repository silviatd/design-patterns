package com.design.patterns.creational.builder.enums;

public enum  Cheese {
    AMERICAN,
    ITALIAN
};
