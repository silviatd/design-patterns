package com.design.patterns.creational.builder.enums;

public enum Dough {
    ITALIAN,
    CLASSIC
}
