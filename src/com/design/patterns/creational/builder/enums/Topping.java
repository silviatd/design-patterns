package com.design.patterns.creational.builder.enums;

public enum Topping {
    PEPPERONI,
    CHICKEN,
    MUSHROOM
}
