package com.design.patterns.creational.factory.models;

public class Chair extends Furniture {
    public Chair(String registrationNumber) {
        super(registrationNumber);
    }
}
