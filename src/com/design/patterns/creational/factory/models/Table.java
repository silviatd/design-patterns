package com.design.patterns.creational.factory.models;

import com.design.patterns.creational.factory.models.Furniture;

public class Table extends Furniture {
    public Table(String registrationNumber) {
        super(registrationNumber);
    }
}
