package com.design.patterns.creational.factory;

import com.design.patterns.creational.factory.models.Chair;
import com.design.patterns.creational.factory.models.Table;

public class BambooFactory implements Factory {
    @Override
    public Table createTable(String registrationNumber) {
        return new Table(registrationNumber);
    }

    @Override
    public Chair createChair(String registrationNumber) {
        return new Chair(registrationNumber);
    }
}
