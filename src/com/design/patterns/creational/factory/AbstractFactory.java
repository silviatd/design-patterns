package com.design.patterns.creational.factory;

import com.design.patterns.creational.factory.models.Furniture;

public class AbstractFactory {
    private Factory factory;

    public AbstractFactory(Factory factory) {
        this.factory = factory;
    }

    public void doSomething() {
        System.out.println("createChair reg: reg-chair-1");
        Furniture chair = factory.createChair("reg-chair-1");

        System.out.println("createTable reg: reg-table-1");
        Furniture table = factory.createTable("reg-table-1");
    }
}