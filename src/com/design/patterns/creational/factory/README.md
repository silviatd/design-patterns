## [Abstract] Factory Design Patterns

Both the Abstract Factory and Factory design pattern are creational design pattern and use to decouple clients from creating objects they need, But there is a significant difference between Factory and Abstract Factory design pattern, Factory design pattern produces implementation of Products e.g. Garment Factory produce different kinds of clothes, On the other hand, Abstract Factory design pattern adds another layer of abstraction over Factory Pattern and Abstract Factory implementation itself like the AbstractFactory will allow you to choose a particular Factory implementation based upon need which will then produce different kinds of products.

