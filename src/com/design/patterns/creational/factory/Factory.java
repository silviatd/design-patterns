package com.design.patterns.creational.factory;

import com.design.patterns.creational.factory.models.Chair;
import com.design.patterns.creational.factory.models.Table;

public interface Factory {
    Table createTable(String registrationNumber);

    Chair createChair(String registrationNumber);
}
