package com.design.patterns.creational.factory;

import com.design.patterns.creational.factory.models.Furniture;

public class Main {
    public static void main(String[] args) {
        AbstractFactory abstractFactory = new AbstractFactory(new BambooFactory());
        abstractFactory.doSomething();
    }
}
