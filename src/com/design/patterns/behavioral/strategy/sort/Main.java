package com.design.patterns.behavioral.strategy.sort;

public class Main {
    public static void main(String[] args) {
        int[] var = {1, 2, 3, 4, 5};
        Sort sort = new Sort(new BubbleSort());
        sort.arrange(var);
    }
}