package com.design.patterns.behavioral.strategy.sort;

class BubbleSort implements Algorithm {
    @Override
    public void sort(int[] numbers) {
        System.out.println("sorting array using bubble sort strategy");
    }
}