package com.design.patterns.behavioral.strategy.sort;

class MergeSort implements Algorithm {
    @Override
    public void sort(int[] numbers) {
        System.out.println("sorting array using merge sort strategy");
    }
}
