package com.design.patterns.behavioral.strategy.sort;

public class Sort {
    private final Algorithm algorithm;

    public Sort(Algorithm strategy) {
        this.algorithm = strategy;
    }

    public void arrange(int[] input) {
        algorithm.sort(input);
    }
}
