package com.design.patterns.behavioral.strategy.sort;

interface Algorithm {
    void sort(int[] numbers);
}