## Strategy Design Pattern

The strategy pattern is used to create an interchangeable family of algorithms from which the required process is chosen at run-time. This allows the behaviour of a program to change dynamically according to configuration details or user preferences. It also increases flexibility by allowing new algorithms to be easily incorporated in the future.

JDK has a couple of examples of this pattern, first is Collections.sort(List, Comparator) method, where Comparator is Strategy and Collections.sort() is Context. Because of this pattern, your sort method can sort any object, the object which doesn't exist when this method was written. As long as, Object will implement the Comparator interface (Strategy interface), Collections.sort() method will sort it.