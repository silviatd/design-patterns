package com.design.patterns.behavioral.strategy.shopping.cart;

public interface PaymentStrategy {
    void pay(int amount);
}
