## Command Design Pattern

The command pattern is a Gang of Four design pattern. This is a behavioural pattern. It is used to implement loose coupling in a request-response model.

The command pattern is useful when supporting activities that require the execution of a series of commands. The command objects can be held in a queue and processed sequentially.