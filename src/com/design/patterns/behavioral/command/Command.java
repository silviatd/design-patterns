package com.design.patterns.behavioral.command;

public interface Command {
    Food execute();
}
