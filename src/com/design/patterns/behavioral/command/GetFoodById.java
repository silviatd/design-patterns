package com.design.patterns.behavioral.command;

public class GetFoodById implements Command {
    @Override
    public Food execute() {
        Food food = new Food();
        System.out.println("Execute getFoodById command");
        return food;
    }
}
