package com.design.patterns.behavioral.command;

public class GetFoodByBarcode implements Command {
    @Override
    public Food execute() {
        Food food = new Food();
        System.out.println("Execute getFoodByBarcode command");
        return food;
    }
}
