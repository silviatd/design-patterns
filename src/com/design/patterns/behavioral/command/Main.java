package com.design.patterns.behavioral.command;

public class Main {
    public static void main(String[] args) {
        Command[] commands = new Command[]{new GetFoodById(), new GetFoodByBarcode()};
        for (Command command : commands) {
            command.execute();
        }
    }
}
