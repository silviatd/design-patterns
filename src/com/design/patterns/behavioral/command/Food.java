package com.design.patterns.behavioral.command;

public class Food {
    private long fdcId;
    private String description;
    private String ingredients;

    public Food() {
        // Empty body
    }

    public Food(long fdcId, String description, String ingredients) {
        this.fdcId = fdcId;
        this.description = description;
        this.ingredients = ingredients;
    }

    public long getFdcId() {
        return fdcId;
    }

    public String getDescription() {
        return description;
    }

    public String getIngredients() {
        return ingredients;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        append(builder, "id: ", String.valueOf(getFdcId()));
        append(builder, "description: ", getDescription());
        append(builder, "ingredients: ", getIngredients());
        return builder.toString();
    }

    private void append(StringBuilder builder, String field, String value) {
        if (value != null && !value.isEmpty()) {
            builder.append(field).append(value).append(System.lineSeparator());
        }
    }
}
