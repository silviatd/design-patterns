package com.design.patterns.behavioral.visitor;

import com.design.patterns.behavioral.visitor.routers.DLinkRouter;
import com.design.patterns.behavioral.visitor.routers.LinkSysRouter;

public interface RouterVisitor {
    void visit(DLinkRouter router);

    void visit(LinkSysRouter router);
}
