package com.design.patterns.behavioral.visitor;

import com.design.patterns.behavioral.visitor.visitors.LinuxConfigurator;
import com.design.patterns.behavioral.visitor.visitors.MacConfigurator;
import com.design.patterns.behavioral.visitor.routers.DLinkRouter;
import com.design.patterns.behavioral.visitor.routers.LinkSysRouter;
import com.design.patterns.behavioral.visitor.routers.Router;

public class Main {
    public static void main(String[] args) {
        MacConfigurator macConfigurator = new MacConfigurator();
        LinuxConfigurator linuxConfigurator = new LinuxConfigurator();

        Router dlink = new DLinkRouter();
        dlink.accept(macConfigurator);

        Router linksys = new LinkSysRouter();
        linksys.accept(linuxConfigurator);
    }
}
