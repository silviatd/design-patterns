package com.design.patterns.behavioral.visitor.visitors;

import com.design.patterns.behavioral.visitor.RouterVisitor;
import com.design.patterns.behavioral.visitor.routers.DLinkRouter;
import com.design.patterns.behavioral.visitor.routers.LinkSysRouter;

public class MacConfigurator implements RouterVisitor {

    @Override
    public void visit(DLinkRouter router) {
        System.out.println("DLinkRouter Configuration for Mac complete !!");
    }

    @Override
    public void visit(LinkSysRouter router) {
        System.out.println("LinkSysRouter Configuration for Mac complete !!");
    }
}