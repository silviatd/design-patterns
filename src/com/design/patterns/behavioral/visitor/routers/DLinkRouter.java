package com.design.patterns.behavioral.visitor.routers;

import com.design.patterns.behavioral.visitor.RouterVisitor;

public class DLinkRouter implements Router {
    @Override
    public void accept(RouterVisitor v) {
        v.visit(this);
    }
}