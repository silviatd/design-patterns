package com.design.patterns.behavioral.visitor.routers;

import com.design.patterns.behavioral.visitor.RouterVisitor;

public interface Router {
    void accept(RouterVisitor v);
}
