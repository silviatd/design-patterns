## Creational
-  [(Abstract) Factory](https://gitlab.com/silviatd/design-patterns/-/tree/master/src/com/design/patterns/creational/factory)
-  [Builder](https://gitlab.com/silviatd/design-patterns/-/tree/master/src/com/design/patterns/creational/builder)
-  [Singleton](https://gitlab.com/silviatd/design-patterns/-/tree/master/src/com/design/patterns/creational/singleton)
  
## Structural
- [Decorator](https://gitlab.com/silviatd/design-patterns/-/tree/master/src/com/design/patterns/structural/decorator)
- [Facade](https://gitlab.com/silviatd/design-patterns/-/tree/master/src/com/design/patterns/structural/facade)
- [Flyweight](https://gitlab.com/silviatd/design-patterns/-/tree/master/src/com/design/patterns/structural/flyweight)
  
## Behavioral
- [Visitor](https://gitlab.com/silviatd/design-patterns/-/tree/master/src/com/design/patterns/behavioral/visitor)
- [Strategy](https://gitlab.com/silviatd/design-patterns/-/tree/master/src/com/design/patterns/behavioral/strategy)
- [Command](https://gitlab.com/silviatd/design-patterns/-/tree/master/src/com/design/patterns/behavioral/command)